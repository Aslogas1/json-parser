import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;


public class Main {
    static final String urlMetro = "https://www.moscowmap.ru/metro.html#lines";
    static TreeMap<String, String> linesMap;
    static TreeMap<String, List<String>> stationsMap;

    public static void main(String[] args) {
        parse(urlMetro);
        JSONUtils.createJsonFile();
        JSONUtils.parseJsonFile();
        for (Map.Entry<String, String> line : JSONUtils.linesFromJson.entrySet()) {
            System.out.println("Линия: " + line.getKey() + " - " + line.getValue());
            for (Map.Entry<String, List<String>> station : JSONUtils.stationsFromJson.entrySet()) {
                if (station.getKey().equals(line.getKey())) {
                    System.out.println("\tКоличество станций: " + station.getValue().size() + ": " +  station.getValue().toString()
                            .replaceAll("\\.", ""));
                }
            }
        }
    }

    public static void parse(String path) {
        try {
            linesMap = new TreeMap<>();
            stationsMap = new TreeMap<>();

            Document doc = Jsoup.connect(path).maxBodySize(0).get();
            Elements lines = doc.select(".js-metro-line");
            lines.forEach(line -> {
                linesMap.put(line.attr("data-line"), line.text());
                Elements stations = doc.select(".js-metro-stations[data-line=" + line.attr("data-line") + "]>p");
                List<String> nameStations = new ArrayList<>();
                stations.forEach(station -> {
                    nameStations.add(station.text().substring(station.text().indexOf(',') + 2));
                });
                stationsMap.put(line.attr("data-line"), nameStations);
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}