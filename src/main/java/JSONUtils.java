import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class JSONUtils {
    private static final String pathJson = "src\\main\\resources\\MetroScheme.json";
    public static TreeMap<String, String> linesFromJson;
    public static TreeMap<String, List<String>> stationsFromJson;

    public static void createJsonFile() {
        try {
            FileWriter file = new FileWriter(pathJson);
            ObjectMapper mapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);
            String jsonString = mapper.writeValueAsString(FillJsonFile());
            file.write(jsonString);
            file.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private static JSONObject FillJsonFile() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("stations", new JSONObject(Main.stationsMap));
        JSONArray jsonArrayLines = new JSONArray();
        for (Map.Entry<String, String> item : Main.linesMap.entrySet()) {
            JSONObject line = new JSONObject();
            line.put("number", item.getKey());
            line.put("name", item.getValue());
            jsonArrayLines.add(line);
        }
        jsonObject.put("lines", jsonArrayLines);
        return jsonObject;
    }

    public static void parseJsonFile() {
        try {
            JSONParser parser = new JSONParser();
            JSONObject jsonData = (JSONObject) parser.parse(getJsonFile());

            JSONArray linesArray = (JSONArray) jsonData.get("lines");
            parseLines(linesArray);

            JSONObject stationsObject = (JSONObject) jsonData.get("stations");
            parseStations(stationsObject);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private static void parseLines(JSONArray linesArray) {
        linesFromJson = new TreeMap<>();
        linesArray.forEach(lineObject -> {
            JSONObject lineJsonObject = (JSONObject) lineObject;
            linesFromJson.put((String) lineJsonObject.get("number"), (String) lineJsonObject.get("name"));
        });
    }

    private static void parseStations(JSONObject stationsObject) {
        stationsFromJson = new TreeMap<>();
        stationsObject.keySet().forEach(lineNumberObject ->
        {
            List<String> nameStation = new ArrayList<>();
            JSONArray stationsArray = (JSONArray) stationsObject.get(lineNumberObject);
            stationsArray.forEach(stationObject ->
            {
                nameStation.add((String) stationObject);
            });
            stationsFromJson.put((String) lineNumberObject, nameStation);
        });
    }

    private static String getJsonFile() {
        StringBuilder builder = new StringBuilder();
        try {
            List<String> lines = Files.readAllLines(Paths.get(pathJson));
            lines.forEach(builder::append);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return builder.toString();
    }

}